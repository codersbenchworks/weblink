@extends('admin.layouts.app')

@section('content')
<div class="container" >
    <div class="row" >
        <div class="col-md-8 col-md-offset-2" style="margin-top: 50px">
        
            <div class="panel panel-default" >
                <div class="panel-heading" align="center"><b>WHATSAPP</b></div>
                <div class="panel-body">

                    <div class="box box-primary">
            <div class="box-body box-profile">
              <form class="form-horizontal" method="POST" 
                    action="{{ url('/whatsappLinkSubmission')}}" enctype="multipart/form-data">
                    @if ($message = Session::get('message'))

                   <div class="alert alert-info alert-block">

                     <button type="button" class="close" data-dismiss="alert">×</button>

                     {{ Session::get('message') }}

                   </div>

                   @endif
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('whatsappLink') ? ' has-error' : '' }}">
                            <label for="whatsappLink" class="col-md-4 control-label">Enter your link </label>

                            <div class="col-md-6">
                                <input id="whatsappLink" type="whatsappLink" class="form-control" name="whatsappLink" value="{{ old('whatsappLink') }}" required autofocus>

                                @if ($errors->has('whatsappLink'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('whatsappLink') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                        </div>
                        
                        </div>
                       
                        </div>
                    </form>

            <!-- /.box-body -->
          </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
