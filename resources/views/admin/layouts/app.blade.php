<!DOCTYPE html>
<html>
@include('admin.layouts.head')
<body>
<body class="hold-transition skin-purple sidebar-mini">
	<div class="wrapper">
		@include('admin.layouts.header')
		@include('admin.layouts.sidebar')
		@section('main-content')
		@show
		@section('content')
		@show
		@include('admin.layouts.footer')
</div>
</body>
</html>