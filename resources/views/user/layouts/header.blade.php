<!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand" href="{{'bloghome'}}"><b>Limits</b> <i>Learn</i></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fas fa-bars"></i>
        </button>
     {{--    <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="index.html">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="about.html">About</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="post.html">Sample Post</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="contact.html">Contact</a>
            </li>
          </ul>
        </div> --}}
      </div>
    </nav>

<header class="masthead" style="background-image: url('{{asset('img/home-bg.jpg')}}')">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <div class="site-heading">
              @foreach($whatsapp as $whatsappLink)

        <p><a target="_blank" href="{{$whatsappLink->link}}" class="btn btn-primary btn-social btn-google btn-flat" style="background-color:#25d366;"><i class="fab fa-whatsapp" ></i>
        Join Whatsapp Group</a></p>
        @endforeach
              {{-- <h1>Clean Blog</h1>
              <span class="subheading">A Blog Theme by Start Bootstrap</span>
 --}}            </div>
          </div>
        </div>
      </div>
    </header>