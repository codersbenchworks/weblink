<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Whatsapp;
use Session;

class whatsappController extends Controller
{
    public function whatsappPage()
    {
    	return view('admin.whatsappPage');
    }

    public function whatsappLinkSubmissionf(Request $request)
 	
 	{
 		$this->validate($request , ['whatsappLink'=>'required',
		]);
		    $input = $request->all();
			$whatsapp = new Whatsapp;
        	$whatsapp->link=$input['whatsappLink'];
			$flag = $whatsapp->save();

        if ($flag) {
            Session::flash('message', 'Uploaded successfully '); 
            Session::flash('alert-class', 'alert-success'); 
  			return redirect('/whatsappLinkView');
        			}
        else        {
            Session::flash('message', 'FAILED to upload'); 
            Session::flash('alert-class', 'alert-danger'); 
       	    return back();
             		} 

 	}
 	 public function whatsappLinkView()
    {
    	$whatsappL = Whatsapp::orderBy("id")->get();
        return view('admin.whatsappLinkView',compact('whatsappL'));
    	// return view('admin.whatsappLinkView');
    }
     public function webblog()
    {
        $whatsapp= Whatsapp::orderBy('id','desc')->limit(1)->get();
        return view('user.userhome',compact('whatsapp'));
    }
}
